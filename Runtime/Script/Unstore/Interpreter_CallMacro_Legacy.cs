﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Interpreter_CallMacro_Legacy : AbstractInterpreterMono
{
    public CommandAuctionExecuter m_executer;
    public MacroCoroutineExecution m_macroExecuter;
    public MacroListRegisterMono m_register;
    public string m_debugLastTranslate;

    public override void TranslateToActionsWithStatus(ref ICommandLine command, ref ExecutionStatus succedToExecute)
    {
        bool succed =TryToTranslate(command);
        
        succedToExecute.SetAsFinished(succed);
    }

    private bool TryToTranslate(ICommandLine command)
    {
      //  Debug.Log("MACROT7EST7>>" + "<<");
        string cmd = command.GetLine();
        int spliterIndex = cmd.IndexOf(":");
        if (spliterIndex <= 0) { return false; }
        if (spliterIndex >= cmd.Length - 1) { return false; }

        string macroInfo = cmd.Substring(0, spliterIndex).ToLower().Replace("macro", "").Trim();
        string macroName = cmd.Substring(
            spliterIndex + 1, cmd.Length - (spliterIndex + 1)).Trim();
        m_debugLastTranslate = macroInfo + " > " + macroName;
        // NamedListOfCommandLines macro;
        // if(!m_register.GetRegister().GetList(macroName, out macro))
        //   return false;
        //List<ICommandLine> cmdLines= macro.GetCommands();

        bool stepbystep = macroInfo.ToLower().IndexOf("stepbystep") >= 0;
        List<ICommandLine> cmds = GetCommandLines(macroName);
       // Debug.Log("MACRO>>" + macroName + "<<"+cmds.Count);
        if (cmds.Count == 0)
            return false;

        if (macroInfo.ToLower().IndexOf("reverse") >= 0)
        {

            CommandLine.Reverse(ref cmds);

        }
        else if (macroInfo.ToLower().IndexOf("random") >= 0)
        {
            CommandLine.Shuffle(ref cmds);
        }
        else { }
        //Debug.Log("What the fuck ??? "+ cmds.Count);
        //for (int i = 0; i < cmds.Count; i++)
        //{
        //    Debug.Log(">" + cmds[i].GetLine());

        //}
       // Debug.Log("MACRO>>" + macroName + "<<");
        if (stepbystep)
           StartCoroutine( m_executer.ExecuteStepByStep(cmds));
        else m_executer.Execute(new ParallelsExecutionCommandLines(cmds, ParallelsExecutionCommandLines.ThreadTriggerType.StartToEnd));
        
        return true;
    }

    private List<ICommandLine> GetCommandLines(string macroName)
    {
     
        List<ICommandLine> cmds;
        if(m_nameToCommandLines.GetRegister().GetCommandLinesOf(macroName, out cmds))
           return cmds;
         if(m_regexToCommandLines.GetRegister().GetCommandLinesOf(macroName, out cmds))
            return cmds;
         return new List<ICommandLine>();
    }

    public StringToCommandLinesRegister m_nameToCommandLines;
    public RegexToCommandsRegisterMono m_regexToCommandLines;

    public override string GetName()
    {
        return "Macro Call";
    }
    public override bool CanInterpreterUnderstand(ref ICommandLine command)
    {
        string cmd = command.GetLine().ToLower().Trim();
        // return command.GetLine().ToLower().IndexOf("call macro ") == 0;
        return cmd.IndexOf("macro") == 0;// && cmd.IndexOf(":") > 0;
    }

    public override void WhatIsYourRequirementFor(ref ICommandLine command, out ICommandExecutioninformation executionInfo)
    {
        executionInfo = m_executionQuote;
    }
    public CommandExecutionInformation m_executionQuote = new CommandExecutionInformation( false, false, true, true);

    public override string WhatWillYouDoWith(ref ICommandLine command)
    {
        return "Try to find a stored macro, group of actions, in the register and execute it.";
    }

}
